<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>Форма</title>
	<meta name="robots" content="nofollow">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>
<body>
	<div class="container-fluid page">
	<header class="header  row">
		<img class="navbar-brand" src="223.png" alt="логотип">
		<p class="float-right name">Форма</p>
		</header>
	<div class="row justify-content-center">
		<div class="col-md-8 content">
			<div class="col order-0 order-sm-1">
				<div id ="messages">
					<?php
						if (!empty($messages)) {
							foreach ($messages as $message) {
								print($message);
								}
							}
					?>
					</div>
						<?php
					if (!empty($_SESSION['login']))
					{
						print ('<div class="exit"><a href="logout.php">Выйти</a></div>');
					}
					?> 
				<form action="index.php" method="POST" class="form">
					<ol>
						<li><label> Имя:<br>
							<input name="field-name-1" class="<?php if ($errors['field-name-1']) print 'error'?>" value="<?php print $values['field-name-1']?>">
							</label></li>
						<li><label>Email:<br>
							<input name="field-e-mail" class="<?php if ($errors['field-e-mail']) {print 'error';} ?>" type="email" value="<?php print $values['field-e-mail']; ?>">
							</label></li>
						<li><label> Год рождения:<br>
							<select name="myselect" value="<?php print $values['myselect']; ?>">
								<?php
									$year = 1960;
									for ($i = 0; $i <= 60; $i++) // Цикл от 0 до 50
									{
										$new_years = $year + $i; // Формируем новое значение
										if ($new_years==$values['myselect']){
											echo '<option selected value='.$new_years.'>'.$new_years.'</option>'; //строка
										}
										else
											echo '<option value='.$new_years.'>'.$new_years.'</option>'; //строка
									}
									?>
								</select>
							</label></li>
						<li>Пол:<br>
							<label><input type="radio" <?php if ($values['radio-group-1']=="Женский") print 'checked="checked"'; ?> name="radio-group-1" value="Женский"> Женский</label>
							<label><input type="radio" <?php if ($values['radio-group-1']=="Мужской") print 'checked="checked"'; ?> name="radio-group-1" value="Мужской"> Мужской</label>
							</li>
						<li>Количество конечностей:
							<br>
							<label><input type="radio" <?php if ($values['radio-group-2']=="4") print 'checked="checked"'; ?> name="radio-group-2" value="4">4</label>
							<label><input type="radio" <?php if ($values['radio-group-2']=="8") print 'checked="checked"'; ?> name="radio-group-2" value="8">8</label>
							<label><input type="radio" <?php if ($values['radio-group-2']=="16") print 'checked="checked"'; ?> name="radio-group-2" value="16">16</label>
							<label><input type="radio" <?php if ($values['radio-group-2']=="5") print 'checked="checked"'; ?> name="radio-group-2" value="5">5</label>
							</li>
						<li>Сверхспособности:<br>
							<select name="field-name-4[]" multiple="multiple"<?php if ($errors['field-name-4']) {print 'class="error"';} ?>>
								<option value="Бессмертие" <?php if (stripos($values['field-name-4'],"Бессмертие")!==FALSE) print ('selected="selected"'); ?>>Бессмертие</option>
								<option value="Прохождение сквозь стены" <?php if (stripos($values['field-name-4'],"Прохождение сквозь стены")!==FALSE) print ('selected="selected"'); ?>>Прохождение сквозь стены</option>
								<option value="Левитация" <?php if (stripos($values['field-name-4'],"Левитация")!==FALSE) print ('selected="selected"'); ?>>Левитация  </option>
								</select>
								
							</li>
						<li><label>Биография:<br>
							<textarea name="field-name-2" class="<?php if ($errors['field-name-2']) {print 'error';} ?>"><?php print $values['field-name-2']; ?></textarea>
							</label></li>
						<li>С контрактом ознакомлен(-а):<br>
							<label><input type="checkbox" checked="checked" name="check-1">Ознакомлен(-а)</label>
							</li>
						</ol>
						<input type="submit" value="Отправить">
						
					</form>
					
					<form action="login.php" method="GET">
					<input type="submit" value="Войти">
					</form>
					<form action="reg.php" method="GET">
					<input type="submit" value="Зарегистрироваться как администратор">
					</form>
					<form action="admin.php" method="GET">
					<input type="submit" value="Войти как администратор">
					</form>
				<br>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid footer">
	<footer>
		© Вероника Холудеева, 2019
		</footer>
	</div>
	</body>
</html>

